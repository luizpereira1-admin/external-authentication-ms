package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;


import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;

public interface Resolver<T extends Throwable> {
    DefaultErrorResponse getErrorResponse(T e);
}
