package br.com.getnet.credenciamentodigital.externalauthenticationms.user.service;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.ConflictException;
import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.ServiceUnavailableException;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain.User;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.ports.CreateUserServicePort;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.ports.SendVerifyEmailPort;
import org.jetbrains.annotations.NotNull;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.List;

@Service
public class CreateUserKeycloakService implements CreateUserServicePort {

    private final Keycloak keycloak;

    private final SendVerifyEmailPort sendVerifyEmail;

    @Value("${keycloak.realm}")
    private String realm;

    @Autowired
    public CreateUserKeycloakService(Keycloak keycloak, SendVerifyEmailPort sendVerifyEmail) {
        this.keycloak = keycloak;
        this.sendVerifyEmail = sendVerifyEmail;
    }

    @Override
    public void create(User user) {
        CredentialRepresentation credential = createCredential(user);
        UserRepresentation userRepresentation = createUserRepresentation(user, credential);

        RealmResource realmResource = this.keycloak.realm(realm);
        UsersResource usersResource = realmResource.users();

        try {
            Response response = usersResource.create(userRepresentation);

            if (response.getStatus() == 409) {
                throw new ConflictException("User already exists");
            }

            if (response.getStatus() != 201) {
                throw new Exception("Unexpected error while creating user");
            }

            user.setId(CreatedResponseUtil.getCreatedId(response));
            this.sendVerifyEmail.send(user);
        } catch (ConflictException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new ServiceUnavailableException("Authentication service is unavailable at this time");
        }
    }

    @NotNull
    private UserRepresentation createUserRepresentation(User user, CredentialRepresentation credential) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(user.getUsername());
        userRepresentation.setEmail(user.getEmail());
        userRepresentation.setEnabled(user.getEnabled());
        userRepresentation.setEmailVerified(user.getEmailVerified());
        userRepresentation.setFirstName(user.getFullname().split(" ", 2)[0]);
        userRepresentation.setLastName(user.getFullname().split(" ", 2)[1]);
        userRepresentation.setCredentials(List.of(credential));


        if (user.getRequiredActions() != null) {
            userRepresentation.setRequiredActions(user.getRequiredActions());
        }

        if (user.getGroups() != null) {
            userRepresentation.setGroups(user.getGroups());
        }

        return userRepresentation;
    }

    @NotNull
    private CredentialRepresentation createCredential(User user) {
        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(user.getPassword());
        credential.setTemporary(true);

        return credential;
    }

}
