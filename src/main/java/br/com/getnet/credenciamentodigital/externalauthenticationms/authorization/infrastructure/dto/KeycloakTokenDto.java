package br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto;

import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.domain.KeycloakToken;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.springframework.validation.annotation.Validated;

@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class KeycloakTokenDto {
    private String accessToken;
    private String refreshToken;

    public KeycloakTokenDto() {
    }

    public KeycloakTokenDto(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public KeycloakTokenDto(KeycloakToken keycloakToken) {
        this.accessToken = keycloakToken.getAccessToken();
        this.refreshToken = keycloakToken.getRefreshToken();
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
