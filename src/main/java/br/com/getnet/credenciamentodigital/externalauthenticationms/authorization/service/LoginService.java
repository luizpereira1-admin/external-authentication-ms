package br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.service;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.ServiceUnavailableException;
import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.UnauthorizedException;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.domain.KeycloakToken;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.ports.ApplicationPort;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto.LoginDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Service
@Validated
public class LoginService implements ApplicationPort {


    @Value("${keycloak.credentials.secret}")
    private String clientSecret;

    @Value("${keycloak.resource}")
    private String clientId;

    @Value("${keycloak.grant-type}")
    private String grantType;

    @Value("${keycloak.auth-server-url}")
    private String authServerUrl;

    @Value("${keycloak.realm}")
    private String realm;

    private RestTemplate restTemplate;

    @Autowired
    public LoginService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public KeycloakToken login(@Valid @NotNull LoginDto loginDto) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("grant_type", grantType);
        map.add("client_id",clientId);
        map.add("client_secret", clientSecret);
        map.add("username", loginDto.getUsername());
        map.add("password", loginDto.getPassword());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        try {
            ResponseEntity <KeycloakToken>
                    response = this.restTemplate.postForEntity(
                            authServerUrl + "/realms/" + realm + "/protocol/openid-connect/token",
                            request,
                            KeycloakToken.class);
            return response.getBody();
        } catch (HttpClientErrorException.Unauthorized e) {
            throw new UnauthorizedException("username or password invalid");
        } catch (Exception e) {
            throw new ServiceUnavailableException("authentication is unavailable at this time");
        }
    }
}
