package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions;

import org.springframework.http.HttpStatus;

public class ConflictException extends ApplicationException {

    public ConflictException() {
        this(null, null, null, null);
    }

    public ConflictException(String detail) {
        this(detail, null, null, null);
    }

    public ConflictException(String detail, String type) {
        this(detail, type, null, null);
    }

    public ConflictException(String detail, String type, String title) {
        this(detail, type, title, null);
    }

    public ConflictException(Throwable cause) {
        this(null, cause);
    }

    public ConflictException(String detail, Throwable cause) {
        this(detail, null, cause);
    }

    public ConflictException(String detail, String type, Throwable cause) {
        this(detail, type, null, cause);
    }

    public ConflictException(String detail, String type, String title, Throwable cause) {
        super(HttpStatus.CONFLICT, detail, type, title, cause);
    }
}
