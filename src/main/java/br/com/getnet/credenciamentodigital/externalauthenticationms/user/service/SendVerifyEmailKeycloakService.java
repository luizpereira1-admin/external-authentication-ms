package br.com.getnet.credenciamentodigital.externalauthenticationms.user.service;

import br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain.User;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.ports.SendVerifyEmailPort;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SendVerifyEmailKeycloakService implements SendVerifyEmailPort {

    private final Keycloak keycloak;

    @Value("${keycloak.realm}")
    private String realm;

    @Autowired
    public SendVerifyEmailKeycloakService(Keycloak keycloak) {
        this.keycloak = keycloak;
    }

    @Override
    public void send(User user) {
        RealmResource realmResource = this.keycloak.realm(realm);
        UsersResource usersResource = realmResource.users();

        UserResource userResource = usersResource.get(user.getId());
        userResource.executeActionsEmail(user.getRequiredActions());
    }

}
