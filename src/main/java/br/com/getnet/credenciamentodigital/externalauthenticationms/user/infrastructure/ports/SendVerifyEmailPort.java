package br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.ports;

import br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain.User;

public interface SendVerifyEmailPort {

    void send(User user);

}
