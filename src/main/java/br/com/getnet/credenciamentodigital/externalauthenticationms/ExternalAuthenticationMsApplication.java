package br.com.getnet.credenciamentodigital.externalauthenticationms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ExternalAuthenticationMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExternalAuthenticationMsApplication.class, args);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

}
