package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;

@Service
public class AccessDeniedExceptionResolver implements Resolver<AccessDeniedException> {

    @Override
    public DefaultErrorResponse getErrorResponse(AccessDeniedException e) {
        return new DefaultErrorResponse(HttpStatus.UNAUTHORIZED, e.getMessage());
    }
}
