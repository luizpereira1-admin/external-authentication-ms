package br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginDto {

    @NotBlank
    @Email
    private String username;

    @NotBlank
    private String password;

    public LoginDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
