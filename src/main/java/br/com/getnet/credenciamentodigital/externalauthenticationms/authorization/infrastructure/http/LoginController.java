package br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.http;


import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.domain.KeycloakToken;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto.KeycloakTokenDto;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.ports.ApplicationPort;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto.LoginDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/security/auth/external")
public class LoginController {
    private final ApplicationPort loginApplication;

    @Autowired
    public LoginController(ApplicationPort loginApplication) {
        this.loginApplication = loginApplication;
    }

    @PostMapping
    public ResponseEntity<KeycloakTokenDto> login(@Valid @RequestBody LoginDto loginDto) {
        KeycloakToken keycloakToken =  loginApplication.login(loginDto);
        return ResponseEntity.ok().body(new KeycloakTokenDto(keycloakToken));
    }
}
