package br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.ports;

import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.domain.KeycloakToken;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto.LoginDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface ApplicationPort {
    KeycloakToken login(@Valid @NotNull LoginDto loginDto);
}
