package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends ApplicationException {

    public UnauthorizedException() {
        this(null, null, null, null);
    }

    public UnauthorizedException(String detail) {
        this(detail, null, null, null);
    }

    public UnauthorizedException(String detail, String type) {
        this(detail, type, null, null);
    }

    public UnauthorizedException(String detail, String type, String title) {
        this(detail, type, title, null);
    }

    public UnauthorizedException(Throwable cause) {
        this(null, cause);
    }

    public UnauthorizedException(String detail, Throwable cause) {
        this(detail, null, cause);
    }

    public UnauthorizedException(String detail, String type, Throwable cause) {
        this(detail, type, null, cause);
    }

    public UnauthorizedException(String detail, String type, String title, Throwable cause) {
        super(HttpStatus.UNAUTHORIZED, detail, type, title, cause);
    }
}
