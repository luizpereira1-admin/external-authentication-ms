package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions;

import org.springframework.http.HttpStatus;

public class ServiceUnavailableException extends ApplicationException {

    public ServiceUnavailableException() {
        this(null, null, null, null);
    }

    public ServiceUnavailableException(String detail) {
        this(detail, null, null, null);
    }

    public ServiceUnavailableException(String detail, String type) {
        this(detail, type, null, null);
    }

    public ServiceUnavailableException(String detail, String type, String title) {
        this(detail, type, title, null);
    }

    public ServiceUnavailableException(Throwable cause) {
        this(null, cause);
    }

    public ServiceUnavailableException(String detail, Throwable cause) {
        this(detail, null, cause);
    }

    public ServiceUnavailableException(String detail, String type, Throwable cause) {
        this(detail, type, null, cause);
    }

    public ServiceUnavailableException(String detail, String type, String title, Throwable cause) {
        super(HttpStatus.SERVICE_UNAVAILABLE, detail, type, title, cause);
    }
}
