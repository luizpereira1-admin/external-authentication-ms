package br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.http;

import br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.dto.UserCreateDto;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.ports.CreateUserServicePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/security/users")
public class UserController {

    private final CreateUserServicePort userService;

    @Autowired
    public UserController(CreateUserServicePort userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody UserCreateDto userCreate) {
        this.userService.create(userCreate.toUser());

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{username}")
                .buildAndExpand(userCreate.getUsername())
                .toUri();

        return ResponseEntity.created(location).build();
    }

}
