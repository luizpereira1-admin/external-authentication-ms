package br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain;

import java.util.List;

public class User {

    private String id;

    private final String username;

    private final String password;

    private final String email;

    private final String fullname;

    private final Boolean enabled;

    private final Boolean emailVerified;

    private final List<String> requiredActions;

    private final List<String> groups;

    public User(
            String username,
            String password,
            String email,
            String fullname,
            Boolean enabled,
            Boolean emailVerified,
            List<String> requiredActions,
            List<String> groups
    ) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.fullname = fullname;
        this.enabled = enabled;
        this.emailVerified = emailVerified;
        this.requiredActions = requiredActions;
        this.groups = groups;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getFullname() {
        return fullname;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public List<String> getRequiredActions() {
        return requiredActions;
    }

    public List<String> getGroups() {
        return groups;
    }

}
