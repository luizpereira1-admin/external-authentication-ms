package br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.dto;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.utils.PasswordRandomGenerate;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserCreateDto {

    @NotNull
    @NotBlank
    @Email
    private final String username;

    @NotNull
    @NotBlank
    private final String fullname;

    private final Boolean enabled;

    private final Boolean emailVerified;

    private final List<String> requiredActions;

    private final List<String> groups;

    public UserCreateDto(
            String username,
            String fullname,
            Boolean enabled,
            Boolean emailVerified,
            List<String> requiredActions,
            List<String> groups
    ) {
        this.username = username;
        this.fullname = fullname;
        this.enabled = enabled == null || enabled;
        this.emailVerified = emailVerified == null || emailVerified;
        this.requiredActions = requiredActions;
        this.groups = groups;
    }

    public String getUsername() {
        return username;
    }

    public User toUser() {
        return new User(
                this.username,
                PasswordRandomGenerate.generate(),
                this.username,
                this.fullname,
                this.enabled,
                this.emailVerified,
                this.requiredActions,
                this.groups
        );
    }

}
