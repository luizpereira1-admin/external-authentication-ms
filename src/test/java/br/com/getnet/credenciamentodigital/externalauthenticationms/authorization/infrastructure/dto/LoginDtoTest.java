package br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LoginDtoTest {

    private String username;
    private String password;
    private LoginDto loginDto;

    @BeforeEach
    void setUp() {
        this.username = "tterezaheloisecatarinadarocha@ftcomercial.com.br";
        this.password = "password";
        this.loginDto = new LoginDto(
                this.username,
                this.password
        );
    }

    @Test
    void getUsernameShouldReturnStringUsernameInformedInInstantiation() {
        assertEquals(this.username, this.loginDto.getUsername());
    }

    @Test
    void getPasswordShouldReturnStringPasswordInformedInInstantiation() {
        assertEquals(this.password, this.loginDto.getPassword());
    }
}