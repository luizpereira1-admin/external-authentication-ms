package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.ApplicationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ApplicationExceptionResolverTest {

    private ApplicationExceptionResolver applicationExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.applicationExceptionResolver = new ApplicationExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        ApplicationException applicationException = Mockito.mock(ApplicationException.class);
        DefaultErrorResponse<?> defaultErrorResponse = Mockito.mock(DefaultErrorResponse.class);

        Mockito.when(defaultErrorResponse.getDetail()).thenReturn("Error");
        Mockito.when(applicationException.getDefaultErrorResponse()).thenReturn(defaultErrorResponse);

        String detail = this.applicationExceptionResolver.getErrorResponse(applicationException).getDetail();
        assertEquals("Error", detail);
    }
}
