package br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.http;

import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.domain.KeycloakToken;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto.KeycloakTokenDto;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto.LoginDto;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.ports.ApplicationPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class LoginControllerTest {

    @Mock
    HttpServletRequest request;

    private LoginDto loginDto;

    private LoginController loginController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        ApplicationPort applicationPort = Mockito.mock(ApplicationPort.class);

        Mockito.when(applicationPort.login(Mockito.any(LoginDto.class))).thenReturn(Mockito.mock(KeycloakToken.class));

        this.loginController = new LoginController(applicationPort);
    }

    @Test
    void loginShouldGenerateTheStatusCodeAndBody() {
        ResponseEntity<KeycloakTokenDto> response = this.loginController.login(
                new LoginDto(
                        "tterezaheloisecatarinadarocha@ftcomercial.com.br",
                        "password")
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }
}