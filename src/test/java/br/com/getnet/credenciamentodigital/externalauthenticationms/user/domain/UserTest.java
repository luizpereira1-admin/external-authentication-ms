package br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    private User user;
    private String username;
    private String password;
    private String email;
    private String fullname;
    private List<String> requiredActions;
    private List<String> groups;
    private String id;

    @BeforeEach
    void setUp() {
        Faker faker = new Faker();

        this.username = faker.name().username();
        this.password = faker.internet().password();
        this.email = faker.internet().safeEmailAddress();
        this.fullname = faker.name().fullName();
        this.requiredActions = List.of(faker.lorem().word(), faker.lorem().word(), faker.lorem().word());
        this.groups = List.of(faker.lorem().word(), faker.lorem().word(), faker.lorem().word());
        this.id = faker.internet().uuid();

        this.user = new User(
                this.username,
                this.password,
                this.email,
                this.fullname,
                true,
                true,
                this.requiredActions,
                this.groups
        );

        this.user.setId(this.id);
    }

    @Test
    void getIdReturnInformedId() {
        assertEquals(this.id, this.user.getId());
    }

    @Test
    void getUsernameReturnInformedUsername() {
        assertEquals(this.username, this.user.getUsername());
    }

    @Test
    void getPasswordReturnInformedPassword() {
        assertEquals(this.password, this.user.getPassword());
    }

    @Test
    void getEmailReturnInformedEmail() {
        assertEquals(this.email, this.user.getEmail());
    }

    @Test
    void getFullnameReturnInformedFullname() {
        assertEquals(this.fullname, this.user.getFullname());
    }

    @Test
    void getEnabledReturnInformedEnabled() {
        assertTrue(this.user.getEnabled());
    }

    @Test
    void getEmailVerifiedReturnInformedEmailVerified() {
        assertTrue(this.user.getEmailVerified());
    }

    @Test
    void getRequiredActionsReturnInformedRequiredActions() {
        assertEquals(this.requiredActions, this.user.getRequiredActions());
    }

    @Test
    void getGroupsReturnInformedGroups() {
        assertEquals(this.groups, this.user.getGroups());
    }

}
