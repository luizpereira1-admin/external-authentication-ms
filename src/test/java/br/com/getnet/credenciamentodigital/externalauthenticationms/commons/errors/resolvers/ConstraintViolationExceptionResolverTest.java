package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import org.hibernate.validator.internal.engine.path.NodeImpl;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConstraintViolationExceptionResolverTest {

    private ConstraintViolationExceptionResolver constraintViolationExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.constraintViolationExceptionResolver = new ConstraintViolationExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponseWithoutConstraintViolations() {
        ConstraintViolationException exception = Mockito.mock(ConstraintViolationException.class);
        Mockito.when(exception.getConstraintViolations()).thenReturn(null);

        DefaultErrorResponse<?> defaultErrorResponse = this.constraintViolationExceptionResolver.getErrorResponse(exception);

        assertEquals("Ocorreu um erro na validação da requisição.", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponseWithConstraintViolations() {
        NodeImpl node = Mockito.mock(NodeImpl.class);
        PathImpl path = Mockito.mock(PathImpl.class);

        Mockito.when(node.getName()).thenReturn("Test");
        Mockito.when(path.getLeafNode()).thenReturn(node);

        ConstraintViolation<?> constraintViolation1 = Mockito.mock(ConstraintViolation.class);
        ConstraintViolationException exception = Mockito.mock(ConstraintViolationException.class);

        Mockito.when(constraintViolation1.getPropertyPath()).thenReturn(path);
        Mockito.when(constraintViolation1.getMessage()).thenReturn("Constraint Violation 1");
        Mockito.when(exception.getConstraintViolations()).thenReturn(Set.of(constraintViolation1));

        DefaultErrorResponse<?> defaultErrorResponse = this.constraintViolationExceptionResolver.getErrorResponse(exception);

        assertEquals("test Constraint Violation 1", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}
