package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HttpMessageNotReadableExceptionResolverTest {

    private HttpMessageNotReadableExceptionResolver httpMessageNotReadableExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.httpMessageNotReadableExceptionResolver = new HttpMessageNotReadableExceptionResolver();
        ReflectionTestUtils.setField(this.httpMessageNotReadableExceptionResolver, "invalidFieldMessage", "error");
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        HttpMessageNotReadableException exception = Mockito.mock(HttpMessageNotReadableException.class);
        Mockito.when(exception.getMessage()).thenReturn("Error");

        DefaultErrorResponse<?> defaultErrorResponse = this.httpMessageNotReadableExceptionResolver.getErrorResponse(exception);

        assertEquals("Error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponseWithJsonMappingException() {
        JsonMappingException.Reference reference = Mockito.mock(JsonMappingException.Reference.class);
        JsonMappingException jsonMappingException = Mockito.mock(JsonMappingException.class);
        HttpMessageNotReadableException exception = Mockito.mock(HttpMessageNotReadableException.class);

        Mockito.when(reference.getFieldName()).thenReturn("test");
        Mockito.when(jsonMappingException.getPath()).thenReturn(List.of(reference));
        Mockito.when(exception.getMessage()).thenReturn("Error");
        Mockito.when(exception.getCause()).thenReturn(jsonMappingException);

        DefaultErrorResponse<?> defaultErrorResponse = this.httpMessageNotReadableExceptionResolver.getErrorResponse(exception);

        assertEquals("test error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}