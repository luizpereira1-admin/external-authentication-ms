package br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.dto;

import br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain.User;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserCreateDtoTest {

    private UserCreateDto userCreateDto;
    private String username;
    private String fullname;
    private List<String> requiredActions;
    private List<String> groups;

    @BeforeEach
    void setUp() {
        Faker faker = new Faker();

        this.username = faker.name().username();
        this.fullname = faker.name().fullName();
        this.requiredActions = List.of(faker.lorem().word(), faker.lorem().word(), faker.lorem().word());
        this.groups = List.of(faker.lorem().word(), faker.lorem().word(), faker.lorem().word());

        this.userCreateDto = new UserCreateDto(
                this.username,
                this.fullname,
                true,
                true,
                this.requiredActions,
                this.groups
        );
    }

    @Test
    void getUsernameReturnInformedUsername() {
        assertEquals(this.username, this.userCreateDto.getUsername());
    }

    @Test
    void toUserReturnUserWithInformedData() {
        User user = this.userCreateDto.toUser();

        assertEquals(this.username, user.getUsername());
        assertEquals(this.username, user.getEmail());
        assertEquals(this.fullname, user.getFullname());
        assertTrue(user.getEnabled());
        assertTrue(user.getEmailVerified());
        assertEquals(this.requiredActions, user.getRequiredActions());
        assertEquals(this.groups, user.getGroups());
    }
}
