package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

class DefaultErrorResponseTest {

    @Test
    void testsTheErrorStructureWithStatusCode() {
        DefaultErrorResponse<Throwable> errorResponse = new DefaultErrorResponse<>(404);

        assertNull(errorResponse.getType());
        assertEquals("Not Found", errorResponse.getTitle());
        assertEquals("Recurso não encontrado.", errorResponse.getDetail());
        assertEquals(404, errorResponse.getStatus());
        assertNull(errorResponse.getOriginalMessage());
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getHttpStatus());
    }

    @Test
    void testsTheErrorStructureWithStatusCodeAndDetail() {
        DefaultErrorResponse<Throwable> errorResponse = new DefaultErrorResponse<>(404, "Detail test.");

        assertNull(errorResponse.getType());
        assertEquals("Not Found", errorResponse.getTitle());
        assertEquals("Detail test.", errorResponse.getDetail());
        assertEquals(404, errorResponse.getStatus());
        assertNull(errorResponse.getOriginalMessage());
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getHttpStatus());
    }

    @Test
    void testsTheErrorStructureWithStatusCodeAndDetailAndType() {
        DefaultErrorResponse<Throwable> errorResponse = new DefaultErrorResponse<>(
                404,
                "Detail test.",
                "Test"
        );

        assertEquals("Test", errorResponse.getType());
        assertEquals("Not Found", errorResponse.getTitle());
        assertEquals("Detail test.", errorResponse.getDetail());
        assertEquals(404, errorResponse.getStatus());
        assertNull(errorResponse.getOriginalMessage());
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getHttpStatus());
    }

    @Test
    void testsTheErrorStructureWithStatusCodeAndDetailAndTypeAndTitle() {
        DefaultErrorResponse<Throwable> errorResponse = new DefaultErrorResponse<>(
                404,
                "Detail test.",
                "Test",
                "Test Not Found"
        );

        assertEquals("Test", errorResponse.getType());
        assertEquals("Test Not Found", errorResponse.getTitle());
        assertEquals("Detail test.", errorResponse.getDetail());
        assertEquals(404, errorResponse.getStatus());
        assertNull(errorResponse.getOriginalMessage());
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getHttpStatus());
    }

    @Test
    void testsTheErrorStructureWithHttpStatusCode() {
        DefaultErrorResponse<Throwable> errorResponse = new DefaultErrorResponse<>(HttpStatus.NOT_FOUND);

        assertNull(errorResponse.getType());
        assertEquals("Not Found", errorResponse.getTitle());
        assertEquals("Recurso não encontrado.", errorResponse.getDetail());
        assertEquals(404, errorResponse.getStatus());
        assertNull(errorResponse.getOriginalMessage());
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getHttpStatus());
    }

    @Test
    void testsTheErrorStructureWithHttpStatusCodeAndDetail() {
        DefaultErrorResponse<Throwable> errorResponse = new DefaultErrorResponse<>(
                HttpStatus.NOT_FOUND,
                "Detail test."
        );

        assertNull(errorResponse.getType());
        assertEquals("Not Found", errorResponse.getTitle());
        assertEquals("Detail test.", errorResponse.getDetail());
        assertEquals(404, errorResponse.getStatus());
        assertNull(errorResponse.getOriginalMessage());
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getHttpStatus());
    }

    @Test
    void testsTheErrorStructureWithHttpStatusCodeAndDetailAndType() {
        DefaultErrorResponse<Throwable> errorResponse = new DefaultErrorResponse<>(
                HttpStatus.NOT_FOUND,
                "Detail test.",
                "Test"
        );

        assertEquals("Test", errorResponse.getType());
        assertEquals("Not Found", errorResponse.getTitle());
        assertEquals("Detail test.", errorResponse.getDetail());
        assertEquals(404, errorResponse.getStatus());
        assertNull(errorResponse.getOriginalMessage());
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getHttpStatus());
    }

    @Test
    void testsTheErrorStructureWithHttpStatusCodeAndDetailAndTypeAndTitle() {
        DefaultErrorResponse<Throwable> errorResponse = new DefaultErrorResponse<>(
                HttpStatus.NOT_FOUND,
                "Detail test.",
                "Test",
                "Test Not Found"
        );

        assertEquals("Test", errorResponse.getType());
        assertEquals("Test Not Found", errorResponse.getTitle());
        assertEquals("Detail test.", errorResponse.getDetail());
        assertEquals(404, errorResponse.getStatus());
        assertNull(errorResponse.getOriginalMessage());
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getHttpStatus());
    }

    @Test
    void throwIllegalArgumentExceptionIfStatusInformedNull() {
        assertThrows(IllegalArgumentException.class, () -> new DefaultErrorResponse<>(null));
    }
}
