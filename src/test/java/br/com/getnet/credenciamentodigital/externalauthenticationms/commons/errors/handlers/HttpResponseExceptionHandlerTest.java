package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.handlers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers.ExceptionResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HttpResponseExceptionHandlerTest {

    private HttpResponseExceptionHandler httpResponseExceptionHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        DefaultErrorResponse<?> defaultErrorResponse = Mockito.mock(DefaultErrorResponse.class);
        ExceptionResolver exceptionResolver = Mockito.mock(ExceptionResolver.class);

        Mockito.when(defaultErrorResponse.getStatus()).thenReturn(404);
        Mockito.when(defaultErrorResponse.getDetail()).thenReturn("Not Found");
        Mockito.when(exceptionResolver.solve(Mockito.any(Throwable.class))).thenReturn(defaultErrorResponse);

        this.httpResponseExceptionHandler = new HttpResponseExceptionHandler(exceptionResolver);
    }

    @Test
    void handleApplicationExceptionGenerateNewDefaultErrorResponse() {
        DefaultErrorResponse<?> defaultErrorResponse = this.httpResponseExceptionHandler.handleApplicationException(
                Mockito.mock(HttpServletResponse.class),
                Mockito.mock(Exception.class)
        );

        assertEquals("Not Found", defaultErrorResponse.getDetail());
        assertEquals(404, defaultErrorResponse.getStatus());
    }
}
