package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HttpRequestMethodNotSupportedExceptionResolverTest {

    private HttpRequestMethodNotSupportedExceptionResolver httpRequestMethodNotSupportedExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.httpRequestMethodNotSupportedExceptionResolver = new HttpRequestMethodNotSupportedExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        HttpRequestMethodNotSupportedException exception = Mockito.mock(HttpRequestMethodNotSupportedException.class);

        DefaultErrorResponse<?> defaultErrorResponse = this.httpRequestMethodNotSupportedExceptionResolver.getErrorResponse(exception);
        assertEquals(405, defaultErrorResponse.getStatus());
    }
}