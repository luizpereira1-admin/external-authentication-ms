package br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.http;

import br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain.User;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.dto.UserCreateDto;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.ports.CreateUserServicePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class UserControllerTest {

    @Mock
    HttpServletRequest request;

    private UserController userController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        CreateUserServicePort servicePort = Mockito.mock(CreateUserServicePort.class);

        this.userController = new UserController(servicePort);
    }

    @Test
    void createMustGenerateTheStatusCodeAndLocationAndBodyNull() {
        UserCreateDto createDto = Mockito.mock(UserCreateDto.class);
        Mockito.when(createDto.toUser()).thenReturn(Mockito.mock(User.class));
        Mockito.when(createDto.getUsername()).thenReturn("test");

        ResponseEntity<?> response = this.userController.create(createDto);

        assertEquals("/test", Objects.requireNonNull(response.getHeaders().get("Location")).get(0));
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNull(response.getBody());
    }
}
