package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MethodArgumentNotValidExceptionResolverTest {

    private MethodArgumentNotValidExceptionResolver methodArgumentNotValidExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.methodArgumentNotValidExceptionResolver = new MethodArgumentNotValidExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponseWithoutBindingResult() {
        MethodArgumentNotValidException exception = Mockito.mock(MethodArgumentNotValidException.class);
        BindingResult bindingResult = Mockito.mock(BindingResult.class);

        Mockito.when(bindingResult.getFieldErrors()).thenReturn(List.of());
        Mockito.when(exception.getBindingResult()).thenReturn(bindingResult);

        DefaultErrorResponse<?> defaultErrorResponse = this.methodArgumentNotValidExceptionResolver.getErrorResponse(exception);

        assertEquals("Ocorreu um erro na validação da requisição.", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponseWithBindingResult() {
        FieldError fieldError = Mockito.mock(FieldError.class);
        MethodArgumentNotValidException exception = Mockito.mock(MethodArgumentNotValidException.class);
        BindingResult bindingResult = Mockito.mock(BindingResult.class);

        Mockito.when(fieldError.getField()).thenReturn("test");
        Mockito.when(fieldError.getDefaultMessage()).thenReturn("error");
        Mockito.when(bindingResult.getFieldErrors()).thenReturn(List.of(fieldError));
        Mockito.when(exception.getBindingResult()).thenReturn(bindingResult);

        DefaultErrorResponse<?> defaultErrorResponse = this.methodArgumentNotValidExceptionResolver.getErrorResponse(exception);

        assertEquals("test error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}
