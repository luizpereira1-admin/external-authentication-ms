package br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.service;

import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.domain.KeycloakToken;
import br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.infrastructure.dto.LoginDto;
import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.ServiceUnavailableException;
import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.UnauthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LoginServiceTest {

    @Mock
    HttpServletRequest request;
    private LoginDto loginDto;
    private LoginService loginService;
    private RestTemplate restTemplate;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        ResponseEntity<KeycloakToken> response = ResponseEntity.ok().body(
                new KeycloakToken(
                        "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.POstGetfAytaZS82wHcjoTyoqhMyxXiWdR7Nn7A29DNSl0EiXLdwJ6xC6AfgZWF1bOsS_TuYI3OG85AmiExREkrS6tDfTQ2B3WXlrr-wp5AokiRbz3_oB4OxG-W9KcEEbDRcZc0nH3L7LzYptiy1PtAylQGxHTWZXtGz4ht0bAecBgmpdgXMguEIcoqPJ1n3pIWk_dUZegpqx0Lka21H6XxUTxiy8OcaarA8zdnPUnV6AmNP3ecFawIFYdvJB_cm-GvpCSbr8G8y_Mllj8f4x9nBH8pQux89_6gUY618iYv7tuPWBFfEbLxtF2pZS6YC1aSfLQxeNe8djT9YjpvRZA",
                        "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI5ODc2NTQzMjEwIiwibmFtZSI6Ik1hcmlhIEFkYW1zIiwiYWRtaW4iOmZhbHNlLCJpYXQiOjEyMzQ1ODc4NDJ9.OJTCQTuYrRB4gim71H3J0SIucQ6q6fnuURQNG0jWJcnCe5FIkP2DNR7RDPxZgbIug8m1S-E9N3ZXOiwJm_OXJoyWciGgd5YqbFS8OskJdOSBPG_zlrX3UF2av8JpOVr7X7n3bhr-QQr7KoB5azuSPGyDf3LRSPsXNdRPkJkwobPVYyV_FEQOGt5DB9zhI9XnxDka6XAdC-pXn_GlkT08XvHi3QkvaajylNhpjK1mlVzbzimkAdnp7U6VHJ4fXHq9juRHly9xRYtH4y7ycX23kuhVEoEInfwj6fY5Nr0qg36teIFc1irNXy40Jow78RiGEtKMOYt7LUdy8bHp2MpkBg"));


        this.restTemplate = Mockito.mock(RestTemplate.class);

        Mockito.when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.any(Class.class))).thenReturn(response);

        this.loginService = new LoginService(restTemplate);
    }

    @Test
    void loginShouldReturnKeycloakTokenWithSuccess() {
        this.loginDto = new LoginDto(
                "tterezaheloisecatarinadarocha@ftcomercial.com.br",
                "passsword"
        );


        KeycloakToken keycloakToken = this.loginService.login(loginDto);

        assertEquals(keycloakToken, new KeycloakToken(
                "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.POstGetfAytaZS82wHcjoTyoqhMyxXiWdR7Nn7A29DNSl0EiXLdwJ6xC6AfgZWF1bOsS_TuYI3OG85AmiExREkrS6tDfTQ2B3WXlrr-wp5AokiRbz3_oB4OxG-W9KcEEbDRcZc0nH3L7LzYptiy1PtAylQGxHTWZXtGz4ht0bAecBgmpdgXMguEIcoqPJ1n3pIWk_dUZegpqx0Lka21H6XxUTxiy8OcaarA8zdnPUnV6AmNP3ecFawIFYdvJB_cm-GvpCSbr8G8y_Mllj8f4x9nBH8pQux89_6gUY618iYv7tuPWBFfEbLxtF2pZS6YC1aSfLQxeNe8djT9YjpvRZA",
                "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI5ODc2NTQzMjEwIiwibmFtZSI6Ik1hcmlhIEFkYW1zIiwiYWRtaW4iOmZhbHNlLCJpYXQiOjEyMzQ1ODc4NDJ9.OJTCQTuYrRB4gim71H3J0SIucQ6q6fnuURQNG0jWJcnCe5FIkP2DNR7RDPxZgbIug8m1S-E9N3ZXOiwJm_OXJoyWciGgd5YqbFS8OskJdOSBPG_zlrX3UF2av8JpOVr7X7n3bhr-QQr7KoB5azuSPGyDf3LRSPsXNdRPkJkwobPVYyV_FEQOGt5DB9zhI9XnxDka6XAdC-pXn_GlkT08XvHi3QkvaajylNhpjK1mlVzbzimkAdnp7U6VHJ4fXHq9juRHly9xRYtH4y7ycX23kuhVEoEInfwj6fY5Nr0qg36teIFc1irNXy40Jow78RiGEtKMOYt7LUdy8bHp2MpkBg"));
    }

    @Test
    void loginShouldThrowsUnauthorizedExceptionWhenLoginCredentialsAreInvalid() {
        Mockito.when(this.restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.any(Class.class))).thenThrow(HttpClientErrorException.Unauthorized.class);
        this.loginDto = new LoginDto(
                "tterezaheloisecatarinadarocha@ftcomercial.com.br",
                "passsword"
        );

        assertThrows(UnauthorizedException.class, () -> this.loginService.login(loginDto));
    }

    @Test
    void loginShouldThrowsServiceUnavailableExceptionWhenCommunicationError() {
        Mockito.when(this.restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.any(Class.class))).thenThrow(RuntimeException.class);
        this.loginDto = new LoginDto(
                "tterezaheloisecatarinadarocha@ftcomercial.com.br",
                "passsword"
        );

        assertThrows(ServiceUnavailableException.class, () -> this.loginService.login(loginDto));
    }
}