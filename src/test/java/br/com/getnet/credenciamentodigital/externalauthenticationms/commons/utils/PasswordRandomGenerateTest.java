package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordRandomGenerateTest {

    @Test
    void generateReturnRandomStringOfEightLength() {
        assertEquals(8, PasswordRandomGenerate.generate().length());
    }

}
