package br.com.getnet.credenciamentodigital.externalauthenticationms.user.service;

import br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SendVerifyEmailKeycloakServiceTest {

    private SendVerifyEmailKeycloakService sendVerifyEmailKeycloakService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        UserResource userResource = Mockito.mock(UserResource.class);
        UsersResource usersResource = Mockito.mock(UsersResource.class);
        RealmResource realmResource = Mockito.mock(RealmResource.class);
        Keycloak keycloak = Mockito.mock(Keycloak.class);

        Mockito.when(usersResource.get(Mockito.anyString())).thenReturn(userResource);
        Mockito.when(realmResource.users()).thenReturn(usersResource);
        Mockito.when(keycloak.realm(Mockito.anyString())).thenReturn(realmResource);

        this.sendVerifyEmailKeycloakService = new SendVerifyEmailKeycloakService(keycloak);

        ReflectionTestUtils.setField(this.sendVerifyEmailKeycloakService, "realm", "external-users");
    }

    @Test
    void sendNotThowsException() {
        User user = Mockito.mock(User.class);
        Mockito.when(user.getId()).thenReturn("1");
        Mockito.when(user.getRequiredActions()).thenReturn(List.of("string"));

        try {
            this.sendVerifyEmailKeycloakService.send(user);
        } catch (Exception exception) {
            fail();
        }
    }

}