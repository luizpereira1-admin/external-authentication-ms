package br.com.getnet.credenciamentodigital.externalauthenticationms.user.service;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.ConflictException;
import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.ServiceUnavailableException;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.domain.User;
import br.com.getnet.credenciamentodigital.externalauthenticationms.user.infrastructure.ports.SendVerifyEmailPort;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import javax.ws.rs.core.Response;

import java.net.URI;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CreateUserKeycloakServiceTest {

    private CreateUserKeycloakService createUserKeycloakService;
    private User user;
    private Response response;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        Faker faker = new Faker();

        this.user = Mockito.mock(User.class);
        Mockito.when(user.getUsername()).thenReturn(faker.name().username());
        Mockito.when(user.getEmail()).thenReturn(faker.internet().safeEmailAddress());
        Mockito.when(user.getFullname()).thenReturn(faker.name().fullName());
        Mockito.when(user.getPassword()).thenReturn(faker.internet().password());
        Mockito.when(user.getEnabled()).thenReturn(true);
        Mockito.when(user.getEmailVerified()).thenReturn(true);
        Mockito.when(user.getRequiredActions()).thenReturn(List.of(faker.lorem().word()));
        Mockito.when(user.getGroups()).thenReturn(List.of(faker.lorem().word()));

        URI uri = Mockito.mock(URI.class);
        this.response = Mockito.mock(Response.class);
        UsersResource usersResource = Mockito.mock(UsersResource.class);
        RealmResource realmResource = Mockito.mock(RealmResource.class);
        Keycloak keycloak = Mockito.mock(Keycloak.class);
        SendVerifyEmailPort sendVerifyEmailPort = Mockito.mock(SendVerifyEmailPort.class);

        Mockito.when(uri.getPath()).thenReturn("http://localhost:users/1");
        Mockito.when(this.response.getStatusInfo()).thenReturn(Response.Status.CREATED);
        Mockito.when(this.response.getStatus()).thenReturn(201);
        Mockito.when(this.response.getLocation()).thenReturn(uri);
        Mockito.when(usersResource.create(Mockito.any(UserRepresentation.class))).thenReturn(response);
        Mockito.when(realmResource.users()).thenReturn(usersResource);
        Mockito.when(keycloak.realm(Mockito.anyString())).thenReturn(realmResource);

        this.createUserKeycloakService = new CreateUserKeycloakService(keycloak, sendVerifyEmailPort);

        ReflectionTestUtils.setField(this.createUserKeycloakService, "realm", "external-users");
    }

    @Test
    void createNotThrowsExceptionIfSuccess() {
        try {
            this.createUserKeycloakService.create(this.user);
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    void createThrowsConflictExceptionWhenCreateUserAlreadyExists() {
        Mockito.when(this.response.getStatus()).thenReturn(409);

        assertThrows(ConflictException.class, () -> this.createUserKeycloakService.create(this.user));
    }

    @Test
    void createThrowsServiceUnavailableExceptionIfErrorInCreateUser() {
        Mockito.when(this.response.getStatus()).thenReturn(400);

        assertThrows(ServiceUnavailableException.class, () -> this.createUserKeycloakService.create(this.user));
    }
}
