package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.bind.MissingServletRequestParameterException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MissingServletRequestParameterExceptionResolverTest {

    private MissingServletRequestParameterExceptionResolver missingServletRequestParameterExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.missingServletRequestParameterExceptionResolver = new MissingServletRequestParameterExceptionResolver();
        ReflectionTestUtils.setField(this.missingServletRequestParameterExceptionResolver, "missingFieldMessage", "error");
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        MissingServletRequestParameterException exception = Mockito.mock(MissingServletRequestParameterException.class);
        Mockito.when(exception.getParameterName()).thenReturn("Error");

        DefaultErrorResponse<?> defaultErrorResponse = this.missingServletRequestParameterExceptionResolver.getErrorResponse(exception);

        assertEquals("error error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}