package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MethodArgumentTypeMismatchExceptionResolverTest {

    private MethodArgumentTypeMismatchExceptionResolver methodArgumentTypeMismatchExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.methodArgumentTypeMismatchExceptionResolver = new MethodArgumentTypeMismatchExceptionResolver();
        ReflectionTestUtils.setField(this.methodArgumentTypeMismatchExceptionResolver, "invalidFieldMessage", "error");
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponseWithMethodArgumentTypeMismatchException() {
        MethodArgumentTypeMismatchException exception = Mockito.mock(MethodArgumentTypeMismatchException.class);

        Mockito.when(exception.getName()).thenReturn("Error");

        DefaultErrorResponse<?> defaultErrorResponse = this.methodArgumentTypeMismatchExceptionResolver.getErrorResponse(exception);

        assertEquals("error error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}