package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.NoHandlerFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NoHandlerFoundExceptionResolverTest {

    private NoHandlerFoundExceptionResolver noHandlerFoundExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.noHandlerFoundExceptionResolver = new NoHandlerFoundExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        NoHandlerFoundException exception = Mockito.mock(NoHandlerFoundException.class);

        DefaultErrorResponse<?> defaultErrorResponse = this.noHandlerFoundExceptionResolver.getErrorResponse(exception);

        assertEquals(404, defaultErrorResponse.getStatus());
    }
}
