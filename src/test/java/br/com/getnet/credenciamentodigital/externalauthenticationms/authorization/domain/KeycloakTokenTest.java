package br.com.getnet.credenciamentodigital.externalauthenticationms.authorization.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class KeycloakTokenTest {

    private String accessToken;
    private String refreshToken;
    private KeycloakToken keycloakToken;

    @BeforeEach
    void setUp() {
        this.accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.POstGetfAytaZS82wHcjoTyoqhMyxXiWdR7Nn7A29DNSl0EiXLdwJ6xC6AfgZWF1bOsS_TuYI3OG85AmiExREkrS6tDfTQ2B3WXlrr-wp5AokiRbz3_oB4OxG-W9KcEEbDRcZc0nH3L7LzYptiy1PtAylQGxHTWZXtGz4ht0bAecBgmpdgXMguEIcoqPJ1n3pIWk_dUZegpqx0Lka21H6XxUTxiy8OcaarA8zdnPUnV6AmNP3ecFawIFYdvJB_cm-GvpCSbr8G8y_Mllj8f4x9nBH8pQux89_6gUY618iYv7tuPWBFfEbLxtF2pZS6YC1aSfLQxeNe8djT9YjpvRZA";
        this.refreshToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI5ODc2NTQzMjEwIiwibmFtZSI6Ik1hcmlhIEFkYW1zIiwiYWRtaW4iOmZhbHNlLCJpYXQiOjEyMzQ1ODc4NDJ9.OJTCQTuYrRB4gim71H3J0SIucQ6q6fnuURQNG0jWJcnCe5FIkP2DNR7RDPxZgbIug8m1S-E9N3ZXOiwJm_OXJoyWciGgd5YqbFS8OskJdOSBPG_zlrX3UF2av8JpOVr7X7n3bhr-QQr7KoB5azuSPGyDf3LRSPsXNdRPkJkwobPVYyV_FEQOGt5DB9zhI9XnxDka6XAdC-pXn_GlkT08XvHi3QkvaajylNhpjK1mlVzbzimkAdnp7U6VHJ4fXHq9juRHly9xRYtH4y7ycX23kuhVEoEInfwj6fY5Nr0qg36teIFc1irNXy40Jow78RiGEtKMOYt7LUdy8bHp2MpkBg";
        this.keycloakToken = new KeycloakToken(
                this.accessToken,
                this.refreshToken
        );
    }

    @Test
    void getAccessTokenShouldReturnStringAccessTokenInformedInInstantiation() {
        assertEquals(this.accessToken, this.keycloakToken.getAccessToken());
    }

    @Test
    void getRefreshTokenShouldReturnStringRefreshTokenInformedInInstantiation() {
        assertEquals(this.refreshToken, this.keycloakToken.getRefreshToken());
    }

    @Test
    void constructNewInstanceSuccessfully() {
        try {
            this.keycloakToken = new KeycloakToken();
        } catch(Exception e) {
            fail();
        }
    }

    @Test
    void equalsShouldReturnTrueComparingTwoEqualKeycloakTokenObjects() {
        assertEquals(
                new KeycloakToken(
                        this.accessToken,
                        this.refreshToken),
                this.keycloakToken);
    }
}