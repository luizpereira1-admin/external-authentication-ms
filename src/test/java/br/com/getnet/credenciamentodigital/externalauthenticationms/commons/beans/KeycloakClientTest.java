package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.beans;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

class KeycloakClientTest {

    private KeycloakClient keycloakClient;

    @BeforeEach
    void setUp() {
        this.keycloakClient = new KeycloakClient();

        ReflectionTestUtils.setField(this.keycloakClient, "serverUrl", "http://localhost");
        ReflectionTestUtils.setField(this.keycloakClient, "realm", "external-users");
        ReflectionTestUtils.setField(this.keycloakClient, "username", "admin");
        ReflectionTestUtils.setField(this.keycloakClient, "password", "admin");
        ReflectionTestUtils.setField(this.keycloakClient, "clientSecret", "secret");
        ReflectionTestUtils.setField(this.keycloakClient, "clientId", "client");
    }

    @Test
    void getKeycloakNotReturnNull() {
        assertNotNull(this.keycloakClient.getKeycloak());
    }

}