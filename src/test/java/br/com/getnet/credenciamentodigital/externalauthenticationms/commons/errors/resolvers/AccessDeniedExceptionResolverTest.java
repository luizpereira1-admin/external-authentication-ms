package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.nio.file.AccessDeniedException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccessDeniedExceptionResolverTest {

    private AccessDeniedExceptionResolver accessDeniedExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.accessDeniedExceptionResolver = new AccessDeniedExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        AccessDeniedException exception = Mockito.mock(AccessDeniedException.class);
        Mockito.when(exception.getMessage()).thenReturn("Access Denied");

        DefaultErrorResponse<?> defaultErrorResponse = this.accessDeniedExceptionResolver.getErrorResponse(exception);

        assertEquals("Access Denied", defaultErrorResponse.getDetail());
        assertEquals(401, defaultErrorResponse.getStatus());
    }

}
