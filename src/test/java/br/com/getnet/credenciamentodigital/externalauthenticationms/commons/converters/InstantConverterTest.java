package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.converters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class InstantConverterTest {

    private InstantConverter converter;

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withZone(ZoneId.systemDefault());

    @BeforeEach
    void setUp() {
        this.converter = new InstantConverter();
    }

    @Test
    void toInstantReturnNullIfNullInformed() {
        assertNull(this.converter.toInstant(null));
    }

    @Test
    void toInstantReturnInstantUsingLocalDate() {
        assertEquals("2021-01-01", FORMATTER.format(this.converter.toInstant("2021-01-01")));
    }

    @Test
    void toInstantReturnInstantUsingLocalDateTime() {
        assertEquals("2021-01-01", FORMATTER.format(this.converter.toInstant("2021-01-01T10:00:00")));
    }

    @Test
    void toInstantReturnInstantUsingInstant() {
        assertEquals("2021-01-01", FORMATTER.format(this.converter.toInstant("2021-01-01T10:00:00.00Z")));
    }

    @Test
    void toInstantThrowsIllegalArgumentExceptionWhenInformedDateNotValid() {
        assertThrows(IllegalArgumentException.class, () -> this.converter.toInstant("01/01/2021"));
    }
}
