package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IllegalArgumentExceptionResolverTest {

    private IllegalArgumentExceptionResolver illegalArgumentExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.illegalArgumentExceptionResolver = new IllegalArgumentExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        IllegalArgumentException exception = Mockito.mock(IllegalArgumentException.class);
        Mockito.when(exception.getMessage()).thenReturn("Error");

        DefaultErrorResponse<?> defaultErrorResponse = this.illegalArgumentExceptionResolver.getErrorResponse(exception);

        assertEquals("Error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}