package br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.resolvers;

import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.domains.DefaultErrorResponse;
import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.ApplicationException;
import br.com.getnet.credenciamentodigital.externalauthenticationms.commons.errors.exceptions.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExceptionResolverTest {

    private ExceptionResolver exceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        DefaultErrorResponse<?> defaultErrorResponse = Mockito.mock(DefaultErrorResponse.class);
        ApplicationExceptionResolver applicationExceptionResolver = Mockito.mock(ApplicationExceptionResolver.class);

        Mockito.when(defaultErrorResponse.getStatus()).thenReturn(400);
        Mockito.when(applicationExceptionResolver.getErrorResponse(Mockito.any(ApplicationException.class)))
                .thenReturn(defaultErrorResponse);

        this.exceptionResolver = new ExceptionResolver(
                applicationExceptionResolver,
                Mockito.mock(ConstraintViolationExceptionResolver.class),
                Mockito.mock(DateTimeParseExceptionResolver.class),
                Mockito.mock(HttpMessageNotReadableExceptionResolver.class),
                Mockito.mock(HttpRequestMethodNotSupportedExceptionResolver.class),
                Mockito.mock(IllegalArgumentExceptionResolver.class),
                Mockito.mock(JsonMappingExceptionResolver.class),
                Mockito.mock(MethodArgumentNotValidExceptionResolver.class),
                Mockito.mock(MethodArgumentTypeMismatchExceptionResolver.class),
                Mockito.mock(MissingRequestHeaderExceptionResolver.class),
                Mockito.mock(MissingServletRequestParameterExceptionResolver.class),
                Mockito.mock(NoHandlerFoundExceptionResolver.class),
                Mockito.mock(AccessDeniedExceptionResolver.class)
        );
    }

    @Test
    void solveReturnDefaultErrorResponseWithResolverIfResolverExists() {
        assertEquals(400, this.exceptionResolver.solve(new BadRequestException("Test")).getStatus());
    }

    @Test
    void solveReturnNewDefaultErrorResponseIfResolverNotExists() {
        assertEquals(500, this.exceptionResolver.solve(new RuntimeException("Test")).getStatus());
    }
}